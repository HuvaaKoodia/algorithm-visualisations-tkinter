import unittest
import sys, os
cPath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(cPath, '../Graph'))
from Graph import *
from TextAlignment import *

class Test(unittest.TestCase):
	def test_Cost(self):
		cost, b, m = AlignText("ABCDE", "badd")
		self.assertEqual(cost, 5)

	def test_M(self):
		cost, _, m = AlignText("Entry", "Sentry")
		m1=len(m)
		m2=len(m[0])
		self.assertEqual(m1, 6)
		self.assertEqual(m2, 7)
		self.assertEqual(m[m1-1][m2-1], cost)

	def test_Align(self):
		_, bestMatch, _ = AlignText("Failures", "Lure")
		self.assertEqual(bestMatch, "***lure*")
