vowels = {"a", "e", "i", "o", "u", "y"}

#Cost setup
mC = 0 #Match letters cost
rC = 1 #Replace letter cost
sC = 2 #Same letter type cost
dC = 3 #Different letter type cost

def MatchCost(l1, l2):
	l1v = l1 in vowels
	l2v = l2 in vowels
	if l1 == l2:
		return mC
	if l1v is not l2v:
		return dC
	return sC

def AlignText(a, b, backtrackCallback=None):
	a = a.lower()
	b = b.lower()

	#Create cost matrix
	height, width = len(a)+1, len(b)+1
	m = [] * height
	for _ in range(height):
		m.append([0]*width)

	#Set initial edge values
	for y in range(height):
		m[y][0] = y * rC
	for x in range(width):
		m[0][x] = x * rC

	#Fill cost matrix
	for y in range(1, height):
		for x in range(1, width):
			m[y][x] = min(MatchCost(a[y-1], b[x-1]) + m[y-1][x-1], rC + m[y-1][x], rC + m[y][x-1])

	#Find best match by backtracking
	y = height-1
	x = width-1
	cost = m[y][x]
	bestMatch = ""

	while (not (x is 0 and y is 0)):
		m1 = m[y-1][x-1]
		m2 = m[y-1][x]
		m3 = m[y][x-1]
		if  m1 <= m2 and m1 <= m3:
			bestMatch += b[x-1]
			x -= 1
			y -= 1
		elif m2 <= m1 and m2 <= m3:
			y -= 1
			bestMatch += "*"
		else:
			x -= 1
			bestMatch += "*"

		if backtrackCallback != None:
			backtrackCallback(bestMatch,x,y)
	return cost, bestMatch[::-1], m
