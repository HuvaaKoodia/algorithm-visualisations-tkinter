import sys
sys.path.append('../Graph')
from Graph import *
from GraphTK import *

from tkinter import *
import math, random
import TextAlignment as ta

#Main containers
root = Tk()
root.title("Text alignment")
canvas = Canvas()
canvas.pack(side=TOP, expand = True, fill=BOTH)

#Data setup
text1 = "maze"
text2 = "amazed"

backtrack=[]
def onBacktrack(bestMatch,x,y):
	backtrack.append((bestMatch,x,y))

cost, aligned, m = ta.AlignText(text1, text2, backtrackCallback = onBacktrack)

#Graph setup
nodeRadius = 20
nodeDiff = nodeRadius*2 + 10
nodeStartX = nodeDiff + nodeDiff
nodeStartY = nodeDiff * len(m)
root.wm_geometry("%dx%d+%d+%d" % (nodeStartX+nodeDiff * len(m[0]), nodeStartY + nodeDiff*3, 25, 25))

graphs = []
coordToIndex = [[0 for x in range(len(m[0]))] for y in range(len(m))]

emptyGraph = Graph()
#texts
for x in range(len(text2)):
	emptyGraph.AddNode(nodeStartX + (x+1) * nodeDiff, nodeStartY + nodeDiff, str(text2[x]), drawBG=False)

for y in range(len(text1)):
	emptyGraph.AddNode(nodeStartX - nodeDiff, nodeStartY - (y+1) * nodeDiff, str(text1[y]), drawBG=False)

graphs.append((emptyGraph.Copy(), f"Text alignment for \"{text1}\" and \"{text2}\"", []))

for y in range(len(m)):
	for x in range(len(m[y])):
		coordToIndex[y][x] = emptyGraph.nodeCount()
		emptyGraph.AddNode(nodeStartX + x * nodeDiff, nodeStartY - y * nodeDiff, "")#str(x)+","+str(y)

graphs.append((emptyGraph.Copy(), "Cost matrix setup", []))

#Add first rows
for x in range(len(m[0])):
	emptyGraph.ChangeNodeValue(coordToIndex[0][x], m[0][x])

for y in range(len(m)):
	emptyGraph.ChangeNodeValue(coordToIndex[y][0], m[y][0])

graphs.append((emptyGraph, f"First row and column have remove costs ({ta.rC} per letter)", []))

#Iterate cost matrix
for y in range(1,len(m)):
	for x in range(1,len(m[y])):
		graph = emptyGraph.Copy()

		for i in range(y+1):
			for j in range(len(m[y])):
				if i == y and j == x:
					break
				graph.ChangeNodeValue(coordToIndex[i][j], m[i][j])

		graphs.append((graph, f"Pair {text1[y-1], text2[x-1]}. Select minimum cost option.", [coordToIndex[y][x]]))
		graphs.append((graph, f"Remove the pair. Cost:{m[y][x-1]}+{ta.rC}={m[y][x-1]+ta.rC}", [coordToIndex[y][x-1]]))
		mc = ta.MatchCost(text1[y-1], text2[x-1])
		graphs.append((graph, f"Keep the pair. Cost:{m[y-1][x-1]}+{mc}={m[y-1][x-1]+mc}", [coordToIndex[y-1][x-1]]))
		graphs.append((graph, f"Remove the pair. Cost:{m[y-1][x]}+{ta.rC}={m[y-1][x]+ta.rC}", [coordToIndex[y-1][x]]))

		graph2 = graph.Copy()
		graph2.ChangeNodeValue(coordToIndex[y][x], m[y][x])
		graphs.append((graph2, f"Best choice cost:{m[y][x]}", [coordToIndex[y][x]]))

#Backtrack
graph = graph2
graphs.append((graph, "Backtrack to find aligned text.", []))

y = len(m)-1
x = len(m[0])-1
graphs.append((graph, "Start from last index in cost matrix.", [coordToIndex[y][x]]))

alignedText=""
for b in backtrack:
	graphs.append((graph, "Select minimum cost option,", [coordToIndex[y][x]]))
	if x-1 >= 0:
		graphs.append((graph, f"{m[y][x-1]} (pair removed)?", [coordToIndex[y][x-1]]))
	if x-1 >= 0 and y-1 >= 0:
		graphs.append((graph, f"{m[y-1][x-1]} (pair kept)?", [coordToIndex[y-1][x-1]]))
	if y-1 >= 0:
		graphs.append((graph, f"{m[y-1][x]} (pair removed)?", [coordToIndex[y-1][x]]))
	x=b[1]
	y=b[2]
	graphs.append((graph, f"Best cost: {m[y][x]}, aligned text: \"{b[0]}\".", [coordToIndex[y][x]]))

graphs.append((graph, f"Lastly, reverse the text to get \"{aligned}\".", []))

#Control panel setup
controls = Frame()
controls.pack(side=BOTTOM, expand = False, fill=BOTH)
label = Label(controls, text="")

#Draw
currentGI = 0
def DrawCurrentGraph():
	ClearCanvas(canvas)
	DrawGraph(graphs[currentGI][0], canvas, nodeRadius)
	SelectNodes(graphs[currentGI][0], canvas, graphs[currentGI][2], nodeRadius)
	label.config(text=str(graphs[currentGI][1]))

def ChangeGraph(c):
	global currentGI
	currentGI = max(min(currentGI + c, len(graphs)-1), 0)
	DrawCurrentGraph()

Grid.rowconfigure(controls, 0, weight=1)
Grid.columnconfigure(controls, 0, weight=1)
Grid.columnconfigure(controls, 1, weight=1)
buttonL=Button(controls, text="Previous", command=lambda :ChangeGraph(-1))
buttonR=Button(controls, text="Next", command=lambda :ChangeGraph(1))
buttonL.grid(row=0, column=0,sticky=W+S+N+E)
buttonR.grid(row=0, column=1,sticky=W+S+N+E)
label.grid(row=1, column=0, columnspan=2,sticky=W+S+N+E)

#Key setup
controls.focus_set()
controls.bind("<Left>", lambda x:ChangeGraph(-1))
controls.bind("<Right>", lambda x:ChangeGraph(1))

#Start program
DrawCurrentGraph()
root.mainloop()
