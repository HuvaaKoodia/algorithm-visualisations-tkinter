import heapq

def BreadthFirstSearchLayers(graph, startID, endID, edgeValidCheck=None):
	index = 0
	layers = []
	layers.append([])
	layers[index].append(startID)
	visited = [False]*len(graph.nodes)
	visited[startID] = True

	#Iterate graph
	while(len(layers[index]) > 0):
		layer = layers[index]
		layers.append([])
		index += 1

		for i in range(len(layer)):
			for c in graph.edgesTable[layer[i]]:
				if not visited[c] and (edgeValidCheck is None or edgeValidCheck(graph.edges[c][2])):
					visited[c] = True
					layers[index].append(c)
					if c == endID:
						return layers

	return layers[:-1] #Remove the last empty layer, in case the endID was not found

def BreadthFirstSearchPath(graph, startID, endID, layers):
	path = [endID]
	edgePath = []
	node = endID

	#Backtrack to find start node
	for i in range(len(layers) - 2, -1, -1):
		for n in layers[i]:
			for e in range(len(graph.edgesTable[n])):
				other = graph.edgesTable[n][e]
				if other == node:
					path.append(n)
					edgePath.append(graph.edgeIndices[n][e])
					node = n
					break
			else:
				continue
			break

	if len(path) == 1:
		return [], []
	return path[::-1], edgePath[::-1]

def DepthFirstSearch(graph, startID, endID, checkNodeCallback = None, checkConnectionsCallback = None):
	stack = []
	visited = [False]*len(graph.nodes)
	parents = [None]*len(graph.nodes)
	stack.append(startID)
	visited[startID] = True

	#Iterate graph
	while(len(stack) > 0):
		node = stack.pop()

		#Callbacks, not needed by the algorith
		if checkNodeCallback != None:
			checkNodeCallback(node)

		if checkConnectionsCallback != None:
			connections = []
			for c in graph.edgesTable[node]:
				if not visited[c]:
					connections.append(c)
			checkConnectionsCallback(node, connections)

		#Checking edges
		for c in graph.edgesTable[node]:
			if not visited[c]:
				visited[c] = True
				parents[c] = node
				if c == endID:
					#Backtrack for path
					path = [c]
					while(parents[c] != None):
						c = parents[c]
						path.append(c)

					#Another callback
					if checkNodeCallback != None:
						checkNodeCallback(endID)
					return path
				stack.append(c)
	return []

def BestFirstSearch(graph, startID, endID, checkNodeCallback = None, checkConnectionsCallback = None):
	pQueue = []
	visited = [-1]*len(graph.nodes)
	parents = [None]*len(graph.nodes)
	heapq.heappush(pQueue, (0, startID))
	visited[startID] = 0

	#Iterate graph
	while(len(pQueue) > 0):
		p = heapq.heappop(pQueue)
		pCost = p[0]
		pNode = p[1]

		#Callbacks, not needed by the algorith
		if checkNodeCallback != None:
			checkNodeCallback(pNode)

		if checkConnectionsCallback != None:
			connetions = []
			for c in graph.edgesTable[pNode]:
				if visited[c] == -1:
					connetions.append(c)
			checkConnectionsCallback(pNode, connetions)

		#Checking edges
		ends = graph.edgesTable[pNode]
		for i in range(len(ends)):
			c = ends[i]
			edge = graph.edges[graph.edgeIndices[pNode][i]]
			cost = pCost + edge[2]
			if visited[c] == -1 or (cost < visited[c]):
				visited[c] = cost
				parents[c] = pNode
				if c == endID:
					#Backtrack for path
					path = [c]
					while(parents[c] != None):
						c = parents[c]
						path.append(c)

					#Another callback
					if checkNodeCallback != None:
						checkNodeCallback(endID)
					return path
				heapq.heappush(pQueue, (cost, c))
	return []
