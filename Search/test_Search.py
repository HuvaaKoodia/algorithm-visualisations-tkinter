import unittest
import sys, os
cPath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(cPath, '../Graph'))
from Search import *
from Graph import *

class TestSearch(unittest.TestCase):
	def InitGraph(self):
		g = Graph()
		size = 50
		g.AddNode(0*size,2*size,0)
		g.AddNode(1*size,3*size,1)
		g.AddNode(1*size,2*size,2)
		g.AddNode(1*size,1*size,3)
		g.AddNode(2*size,2*size,4)
		g.AddNode(3*size,1*size,5)
		g.AddNode(3*size,0*size,6)

		g.AddEdge(0, 1, value=1)
		g.AddEdge(1, 4, value=1)
		g.AddEdge(1, 2, value=1)
		g.AddEdge(2, 3, value=1)
		g.AddEdge(4, 5, value=1)
		g.AddEdge(3, 5, value=1)
		g.AddEdge(5, 6, value=1)
		return g

	def test_BreadthFirst(self):
		g = self.InitGraph()

		layers = BreadthFirstSearchLayers(g, 0, 6)
		path, edgePath = BreadthFirstSearchPath(g, 0, 6, layers)

		self.assertEqual(len(path), 5)
		self.assertEqual(len(edgePath), 4)
		self.assertTrue(4 in path)

	def test_DepthFirst(self):
		g = self.InitGraph()

		path = DepthFirstSearch(g, 0, 6)

		self.assertEqual(len(path), 6)
		self.assertFalse(4 in path)

	def test_BestFirst(self):
		g = self.InitGraph()

		path = BestFirstSearch(g, 0, 6)

		self.assertEqual(len(path), 5)
		self.assertTrue(4 in path)
