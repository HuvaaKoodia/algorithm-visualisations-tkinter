import sys, math
sys.path.append('../Graph')
from Graph import *
from GraphTK import *

from tkinter import *
from Search import *

#Main containers
root = Tk()
root.title("Search")
root.wm_geometry("%dx%d+%d+%d" % (650, 650, 25, 25))
canvas = Canvas()
canvas.pack(expand = True, fill=BOTH)

#Setup
nodeRadius = 25
graph = None
currentL = 0
layers = []
layerTexts = []
path = []
selectedAlgorithm = IntVar()

def Generate():
	global graph
	graph = Graph()
	graph.CreateRandomNodes(10, nodeRadius * 2, width=600, height=600)
	graph.CreateCloseEdges(2, nodeRadius + 10)

	#Edge distances
	for i in range(graph.edgeCount()):
		n1 = graph.edges[i][0]
		n2 = graph.edges[i][1]
		graph.ChangeEdgeValue(i, math.ceil(graph.DistanceNode(n1,n2)/10))

def Reset():
	global currentL, label, layers, layerTexts, path
	currentL = 0
	label.config(text="")
	layerTexts = []

	#Search setup
	startID = 0
	layers = BreadthFirstSearchLayers(graph, startID, -1)
	endID = layers[-1][0] #Ends in a node far away from start

	#Run selected search algorithm
	if selectedAlgorithm.get() == 0:
		#Breadth First Search
		path, _ = BreadthFirstSearchPath(graph, startID, endID, layers)
		layerTexts.append("Add start node to the first layer.")
		for l in range(len(layers)-1):
			layerTexts.append("Add unvisited connected nodes to the next layer.")
		layerTexts.append("Backtrack through layers to find the path.")
	elif selectedAlgorithm.get() == 1:
		#Depth first search
		layers = [[startID]]
		layerTexts.append("Add start node to stack.")
		def checkNode(n):
			layers.append([n])
			layerTexts.append("Check top node from stack.")
		def checkConnections(n, c):
			if len(c) == 0:
				layers.append([n])
				layerTexts.append("Current node has no unvisited connections.")
			else:
				layers.append(c)
				layerTexts.append("Add unvisited connected nodes to stack.")
		path = DepthFirstSearch(graph, startID, endID, checkNodeCallback=checkNode, checkConnectionsCallback=checkConnections)
		layerTexts.append("Backtrack through node parents to find the path.")
	else:
		#Best first search
		layers = [[startID]]
		layerTexts.append("Add start node to priority queue.")

		def checkNode(n):
			layers.append([n])
			layerTexts.append("Check top node from priority queue.")
		def checkConnections(n, c):
			if len(c) == 0:
				layers.append([n])
				layerTexts.append("Current node has no unvisited connections.")
			else:
				layers.append(c)
				layerTexts.append("Add unvisited connected nodes to priority queue based on distance from node 0.")
		path = BestFirstSearch(graph, startID, endID, checkNodeCallback=checkNode, checkConnectionsCallback=checkConnections)
		layerTexts.append("Backtrack through node parents to find the path.")

	ClearCanvas(canvas)
	DrawGraph(graph, canvas, nodeRadius)
	DrawCurrentLayer()

def Regenerate():
	Generate()
	Reset()

#Drawing
def DrawLayer(index):
	DeleteSelection(canvas)
	SelectNodes(graph, canvas, layers[index], nodeRadius)

def DrawPath():
	DeleteSelection(canvas)
	SelectNodes(graph, canvas, path, nodeRadius, color="blue")
	SelectEdges(graph, canvas, path, color="blue")

def DrawCurrentLayer():
	if currentL == len(layers):
		DrawPath()
	else:
		DrawLayer(currentL)
	label.config(text=layerTexts[currentL])

def ChangeLayer(c):
	global currentL
	currentL = max(min(currentL + c, len(layers)), 0)
	DrawCurrentLayer()

#Control panel setup
frame = Frame()
frame.pack(side=BOTTOM, expand = False, fill=BOTH)

#Widgets
label = Label(frame, text="")
buttonG=Button(frame, text="Generate", command=Regenerate)
buttonP=Button(frame, text="Previous", command=lambda :ChangeLayer(-1))
buttonN=Button(frame, text="Next", command=lambda :ChangeLayer(1))
BFS = Radiobutton(frame, text="Breadth first search", variable = selectedAlgorithm, value = 0, command=Reset)
DFS = Radiobutton(frame, text="Depth first search", variable = selectedAlgorithm, value = 1, command=Reset)
BestFS = Radiobutton(frame, text="Best first search", variable = selectedAlgorithm, value = 2, command=Reset)

#Grid layout
Grid.rowconfigure(frame, 0, weight=1)
Grid.rowconfigure(frame, 1, weight=1)
Grid.rowconfigure(frame, 2, weight=1)
Grid.columnconfigure(frame, 0, weight=1)
Grid.columnconfigure(frame, 1, weight=1)
Grid.columnconfigure(frame, 2, weight=1)
label.grid(row=0, column=0, columnspan=3,sticky=W+S+N+E)
buttonG.grid(row=1, column=0,sticky=W+S+N+E)
buttonP.grid(row=1, column=1,sticky=W+S+N+E)
buttonN.grid(row=1, column=2,sticky=W+S+N+E)
BFS.grid(row=2, column = 0, sticky=W+S+N+E)
DFS.grid(row=2, column = 1, sticky=W+S+N+E)
BestFS.grid(row=2, column = 2, sticky=W+S+N+E)

#Key setup
frame.focus_set()
frame.bind("g", lambda x:Regenerate())
frame.bind("<Left>", lambda x:ChangeLayer(-1))
frame.bind("<Right>", lambda x:ChangeLayer(1))

#Run
Regenerate()
root.mainloop()
