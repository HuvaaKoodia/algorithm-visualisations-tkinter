# Algorithm visualisations TKinter

Algorithm visualisations with Python 3 and TKinter.

Includes visualisations for:
- Heap
- Network flow
- Text alignment (edit distance)
- Graph path search (breadth first / depth first / best first)

![](_Images/Search.png)

## Requirements

- [Python 3](https://www.python.org)
- [TKinter](https://docs.python.org/3/library/tkinter.html)

## Usage

There is a *GUI.py* script in each folder, run it directly:

```
python3 GUI.py
```

Or run the *RunGUI.sh* instead (on Linux):
```
./RunGUI.py
```

## License

MIT License