import sys
sys.path.append('../Graph')
from Graph import *
from GraphTK import *

from tkinter import *
import math, random
from Heap import Heap

#Main containers
root = Tk()
root.title("Heap")
canvas = Canvas()
canvas.pack(side=TOP, expand = True, fill=BOTH)

#Data setup
nodeRadius = 20
xDiff = (nodeRadius * 2) + 10
array = [i for i in range(15)]
random.shuffle(array)

#Create heaps (add all, remove all)
heaps = []
heap = Heap(len(array))
maxDepth = 0
for i in range(len(array)):
	value = array[i]
	heap.Add(value)
	heaps.append((heap,f"Added {value}"))
	heap = heap.Copy()
	depth = math.floor(math.log(heap.size, 2))
	if depth > maxDepth:
		maxDepth = depth

for i in range(len(array)-1):
	value = heap.Peek()
	heap.RemoveTop()
	heaps.append((heap,f"Removed {value}"))
	heap = heap.Copy()

#Create graphs
graphs = []
biggestX = 0
startXDiff = maxDepth*xDiff

for k in heaps:
	h = k[0]
	size = h.size
	depth = math.floor(math.log(size, 2))
	graph = Graph()
	graphs.append(graph)

	x = 10+nodeRadius*2+xDiff*depth*2*2
	if x > biggestX:
		biggestX = x

	def CreateGraphR(i, d, startX=30, startY=30, parent = -1):
		pi = len(graph.nodes)
		graph.AddNode(startX, startY, h.array[i])

		if parent >= 0:
			graph.AddEdge(parent, pi)

		ci = i*2
		if ci <= size:
			CreateGraphR(ci, d*0.5, startX-d, startY + 50, pi)

		ci = i*2+1
		if ci <= size:
			CreateGraphR(ci, d*0.5, startX+d, startY + 50, pi)

	CreateGraphR(1, startXDiff, nodeRadius*2 + startXDiff*2)

#Set window size
root.wm_geometry("%dx%d+%d+%d" % (biggestX, 400, 25, 25))

#Draw
currentGI = 0
def DrawCurrentGraph():
	ClearCanvas(canvas)
	DrawGraph(graphs[currentGI], canvas, nodeRadius)

DrawCurrentGraph()

#Control panel setup
frame = Frame()
frame.pack(side=BOTTOM, expand = False, fill=BOTH)
label = Label(frame, text="")

def ChangeGraph(c):
	global currentGI
	currentGI = max(min(currentGI + c, len(graphs)-1), 0)
	DrawCurrentGraph()
	label.config(text=heaps[currentGI][1])

Grid.rowconfigure(frame, 0, weight=1)
Grid.columnconfigure(frame, 0, weight=1)
Grid.columnconfigure(frame, 1, weight=1)
buttonL =Button(frame, text="Previous", command=lambda :ChangeGraph(-1))
buttonR =Button(frame, text="Next", command=lambda :ChangeGraph(1))
buttonL.grid(row=0, column=0,sticky=W+S+N+E)
buttonR.grid(row=0, column=1,sticky=W+S+N+E)
label.grid(row=1, column=0, columnspan=2,sticky=W+S+N+E)

#Key setup
frame.focus_set()
frame.bind("<Left>", lambda x:ChangeGraph(-1))
frame.bind("<Right>", lambda x:ChangeGraph(1))

root.mainloop()
