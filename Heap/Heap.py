#Uses 1 as first index. Array position 0 unused as a result.
class Heap:
	def __init__(self, capacity):
		self.size = 0
		self.array = [-1]*(capacity + 1)

	def Copy(self):
		copy = Heap(len(self.array))
		copy.size = self.size
		copy.array = [] + self.array
		return copy

	def Peek(self):
		return self.array[1]

	def Add(self, value):
		self.size += 1
		self.array[self.size] = value
		self.HeapifyUp(self.size)

	def RemoveTop(self):
		if self.size <= 1:
			self.array[1] = -1
			self.size = 0
			return
		self.array[1] = self.array[self.size]
		self.size -= 1
		self.HeapifyUp(1)
		self.HeapifyDown(1)

	def HeapifyUp(self, index):
		if index == 1:
			return

		pindex = index // 2
		if self.array[index] < self.array[pindex]:
			self.array[index], self.array[pindex] = self.array[pindex], self.array[index]
			self.HeapifyUp(pindex)

	def HeapifyDown(self, index):
		cindex = index * 2
		if cindex > self.size:
			return
		if cindex != self.size:
			if self.array[cindex + 1] < self.array[cindex]:
				cindex += 1

		if self.array[index] > self.array[cindex]:
			self.array[index], self.array[cindex] = self.array[cindex], self.array[index]
			self.HeapifyDown(cindex)
