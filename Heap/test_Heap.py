import unittest
from Heap import *

class TestHeap(unittest.TestCase):
	def test_Add(self):
		heap = Heap(10)
		heap.Add(4)
		self.assertEqual(heap.Peek(), 4)
		heap.Add(5)
		self.assertEqual(heap.Peek(), 4)
		heap.Add(3)
		self.assertEqual(heap.Peek(), 3)
		heap.Add(10)
		self.assertEqual(heap.Peek(), 3)
		heap.Add(1)
		self.assertEqual(heap.Peek(), 1)

	def test_Copy(self):
		heap = Heap(10)
		copy = heap.Copy()
		self.assertEqual(heap.array, copy.array)
		self.assertEqual(heap.size, copy.size)

	def test_Remove(self):
		heap = Heap(10)
		heap.Add(4)
		heap.Add(5)
		heap.Add(3)
		heap.Add(10)
		heap.Add(1)

		heap.RemoveTop()
		self.assertEqual(heap.Peek(), 3)
		heap.RemoveTop()
		self.assertEqual(heap.Peek(), 4)
		heap.RemoveTop()
		self.assertEqual(heap.Peek(), 5)
		heap.RemoveTop()
		self.assertEqual(heap.Peek(), 10)
		heap.RemoveTop()
		self.assertEqual(heap.Peek(), -1)
