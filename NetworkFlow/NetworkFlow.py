import sys, math, os
cPath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(cPath, '../Search'))
from Search import *

def SolveNetworkFlow(graph, source, destination, useScaling=False, augmentedGraphCallback=None, scaleChangedCallback=None):
	iteration = 0
	maxFlow = 0
	if useScaling:
		maxInC = 0
		for e in graph.edgeIndices[source]:
			if graph.edges[e][2] > maxInC:
				maxInC = graph.edges[e][2]
		scale = 2**math.floor(math.log(maxInC,2))
	else:
		scale = 0
	while(True):
		flowPerEdge=[0]*len(graph.edges)
		layers = BreadthFirstSearchLayers(graph, source, destination, edgeValidCheck = lambda v:v>=scale)
		path, edgePath = BreadthFirstSearchPath(graph, source, destination, layers)
		#No flow can get through the graph
		if len(path)==0 and scale <= 1: break
		#When scaling lets reduce the scale factor and try again
		if len(path)==0 and scale > 1:
			if scale > 1:
				scale /= 2
			if scaleChangedCallback != None:
				scaleChangedCallback(graph, scale*2, scale)
			continue
		iteration += 1

		#Find minimum path edge capacity AKA bottleneck
		minPathCapacity = sys.maxsize
		for e in edgePath:
			edge = graph.edges[e]
			capacity = edge[2]
			if capacity < minPathCapacity:
				minPathCapacity = capacity

		#Apply flow on path
		for e in edgePath:
			flowPerEdge[e] = minPathCapacity
		maxFlow += minPathCapacity

		#Augment graph
		aGraph = graph.Copy(copyEdges=False)

		def AddOrCombineEdge(start, end, value):
			for i in range(len(aGraph.edgesTable[start])):
				if aGraph.edgesTable[start][i] == end:
					#Sometimes two edges can be added in the same direction. Better to combine them.
					endID = aGraph.edgeIndices[start][i]
					aGraph.ChangeEdgeValue(endID, aGraph.edges[endID][2]+value)
					break
			else:
				#No duplicate edge, just create it.
				aGraph.AddEdge(start, end, value, True)

		#Keep or flip edges based on flow
		for e in range(len(graph.edges)):
			edge = graph.edges[e]
			capacity = edge[2]
			if flowPerEdge[e] == 0:#Unused edge, keep as it is
				AddOrCombineEdge(edge[0], edge[1], capacity)
			else:
				capacityUnused = capacity - flowPerEdge[e]
				if capacityUnused > 0:#Unused capacity, keep at that reduced capacity
					AddOrCombineEdge(edge[0], edge[1], capacityUnused)
				#Used capacity, create new edge in opposite  direction
				AddOrCombineEdge(edge[1], edge[0], flowPerEdge[e])

		if augmentedGraphCallback is not None:
			augmentedGraphCallback(aGraph.Copy(), iteration, path, scale)

		graph = aGraph

	return maxFlow, iteration
