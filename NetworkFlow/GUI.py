import sys
sys.path.append('../Graph')
from Graph import *
from GraphTK import *

from tkinter import *
import math, random
from NetworkFlow import *

#Main containers
root = Tk()
root.title("Network flow")
canvas = Canvas()
canvas.pack(side=TOP, expand = True, fill=BOTH)

#Data setup
selectedGraphType = IntVar()
nodeRadius = 20
nodesY=5
layerCount = 2
startX=200
startY=50
diffX=10+nodeRadius*8
diffY=10+nodeRadius*4
graphs = []
currentGraphType = -1
baseGraph = None
inFlow = 0
outFlow = 0
source = 0
destination = 0

def Generate():
	global graphs, layerCount, currentGraphType, baseGraph, inFlow, outFlow, source, destination

	#New graph generation
	selectedType = selectedGraphType.get()
	if currentGraphType < 0 or (currentGraphType is 0 and selectedType is not 0) or (currentGraphType > 0 and selectedType is 0):
		baseGraph = Graph()
		#Graph stats
		if selectedType == 0:
			layerCount = 2
			edgeCountRange = (1,2)
			edgeValueRange = (1,1)
			edgeInRange = (1,1)
			edgeOutRange = (1,1)
		elif selectedType > 0:
			layerCount = 3
			edgeCountRange = (2,3)
			edgeValueRange = (1,10)
			edgeInRange = (10,20)
			edgeOutRange = (10,20)

		currentGraphType = selectedType

		#Create nodes
		layers = baseGraph.CreateNodeLayers(layerCount,nodesY,startX,startY,diffX,diffY, setNodeValuesToIndices=False)
		source = baseGraph.AddNode(startX*0.35,startY+diffY*(nodesY-1)/2,"s")
		destination = baseGraph.AddNode(startX + diffX*(layerCount-1) + startX*0.65,startY+diffY*(nodesY-1)/2,"d")

		#Create edges
		baseGraph.CreateRandomLayerEdges(layers, edgeCount=edgeCountRange, randomValues=edgeValueRange, directed=True)
		inFlow = 0
		outFlow = 0
		for n in layers[0]:
			flow = random.randint(edgeInRange[0],edgeInRange[1])
			inFlow += flow
			baseGraph.AddEdge(source, n, value=flow, directed=True)
		for n in layers[-1]:
			flow = random.randint(edgeOutRange[0],edgeOutRange[1])
			outFlow += flow
			baseGraph.AddEdge(n, destination, value=flow, directed=True)

	#Algorithm iterations setup
	graphs = []
	graphs.append((baseGraph, f"Setup: in flow {inFlow}, out flow {outFlow}", []))

	def onAugmentedGraph(aGraph, iteration, path, scale):
		if scale is 0:
			graphs.append((aGraph, f"Augmented graph {iteration}", path))
		else:
			graphs.append((aGraph, f"Augmented graph {iteration}, scale value: {scale}", path))

	def onScaleValueChange(aGraph, oldScale, newScale):
		graphs.append((aGraph, f"No more paths with scaling value {oldScale}. Halving scale value to {newScale}", []))

	maxFlow, iterations = SolveNetworkFlow(baseGraph, source, destination, useScaling = selectedType is 2, augmentedGraphCallback = onAugmentedGraph, scaleChangedCallback=onScaleValueChange)
	graphs.append((graphs[-1][0], f"Max flow {maxFlow}, iterations {iterations}", []))

#Control panel setup
controls = Frame()
controls.pack(side=BOTTOM, expand = False, fill=BOTH)
label = Label(controls, text="")

#Draw
currentGI = 0
def DrawCurrentGraph():
	global graphs
	ClearCanvas(canvas)
	DrawGraph(graphs[currentGI][0], canvas, nodeRadius, alwaysShowEdgeArrows=True)
	SelectEdges(graphs[currentGI][0], canvas, graphs[currentGI][2], color="blue", width=2)
	label.config(text=str(graphs[currentGI][1]))

def ChangeGraph(c):
	global currentGI
	currentGI = max(min(currentGI + c, len(graphs)-1), 0)
	DrawCurrentGraph()

def Reset():
	global currentGI
	Generate()
	#Window size to graph size
	root.wm_geometry("%dx%d+%d+%d" % (startX*2+diffX*(layerCount-1), 600, 25, 25))
	currentGI = 0
	DrawCurrentGraph()

def Regenerate():
	global currentGraphType #Quick hack
	currentGraphType = -1
	Reset()

#Control widget setup
buttonG=Button(controls, text="Generate", command=Regenerate)
buttonL=Button(controls, text="Previous", command=lambda :ChangeGraph(-1))
buttonR=Button(controls, text="Next", command=lambda :ChangeGraph(1))
radioB=Radiobutton(controls, text="Biparted", variable=selectedGraphType, value = 0, command=Reset)
radioM=Radiobutton(controls, text="Multilayer", variable=selectedGraphType, value = 1, command=Reset)
radioS=Radiobutton(controls, text="Multilayer scaled", variable=selectedGraphType, value = 2, command=Reset)

#Layout
Grid.rowconfigure(controls, 0, weight=1)
Grid.columnconfigure(controls, 0, weight=1)
Grid.columnconfigure(controls, 1, weight=1)
Grid.columnconfigure(controls, 2, weight=1)
label.grid(row=0, column=0, columnspan=3,sticky=W+S+N+E)
buttonG.grid(row=1, column=0,sticky=W+S+N+E)
buttonL.grid(row=1, column=1,sticky=W+S+N+E)
buttonR.grid(row=1, column=2,sticky=W+S+N+E)
radioB.grid(row=2, column=0)
radioM.grid(row=2, column=1)
radioS.grid(row=2, column=2)

#Hotkey setup
controls.focus_set()
controls.bind("<Left>", lambda x:ChangeGraph(-1))
controls.bind("<Right>", lambda x:ChangeGraph(1))
controls.bind("g", lambda x:Regenerate())

#Start program
Reset()
root.mainloop()
