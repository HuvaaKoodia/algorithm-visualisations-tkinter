import unittest
import sys, os
cPath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(cPath, '../Graph'))
from Graph import *
from NetworkFlow import *

class TestSearch(unittest.TestCase):
	def InitGraph(self):
		g = Graph()

		#Two layers with 3 nodes in each
		nodeRadius = 20
		s = nodeRadius
		d = nodeRadius*2
		source = g.AddNode(s+d*0,s+d*0)#0

		g.AddNode(s+d*2,s+d*0)#1
		g.AddNode(s+d*2,s+d*1)#2
		g.AddNode(s+d*2,s+d*2)#3

		g.AddNode(s+d*4,s+d*0)#4
		g.AddNode(s+d*4,s+d*1)#5
		g.AddNode(s+d*4,s+d*2)#6

		destination = g.AddNode(s+d*6,s+d*0)#7

		g.AddEdge(0,1,value=3, directed=True)
		g.AddEdge(0,2,value=2, directed=True)
		g.AddEdge(0,3,value=4, directed=True)

		g.AddEdge(1,4,value=3, directed=True)
		g.AddEdge(1,6,value=2, directed=True)

		g.AddEdge(2,4,value=3, directed=True)
		g.AddEdge(2,5,value=2, directed=True)

		g.AddEdge(3,5,value=3, directed=True)
		g.AddEdge(3,6,value=2, directed=True)

		g.AddEdge(3,7,value=3, directed=True)
		g.AddEdge(4,7,value=2, directed=True)
		g.AddEdge(5,7,value=4, directed=True)

		return g, source, destination

	def test_MaxFlow(self):
		g, s, d = self.InitGraph()

		maxFlow, iterations = SolveNetworkFlow(g, s, d)

		self.assertEqual(maxFlow, 8)
		self.assertEqual(iterations, 4)
