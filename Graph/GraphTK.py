from LineSegmentIntersection import *

#Drawing the graph
def DrawGraph(graph, canvas, nodeRadius = 20, alwaysShowEdgeArrows=False):
	for e in graph.edges:
		n1 = graph.nodes[e[0]]
		n2 = graph.nodes[e[1]]

		#Line
		lineColor = e[3]
		x1 = n1[1][0]
		y1 = n1[1][1]
		x2 = n2[1][0]
		y2 = n2[1][1]
		v1 = (x2-x1,y2-y1,0)
		v1m = mag(v1)
		v1n = norm(v1)
		v1m = v1m-nodeRadius
		x2 = x1 + v1n[0]*v1m
		y2 = y1 + v1n[1]*v1m
		canvas.create_line(x1,y1,x2,y2, fill=lineColor, tag="edge")

		#Arrow for directional edges
		if alwaysShowEdgeArrows or (not e[0] in graph.edgesTable[e[1]]):
			size = 5
			ar = mul(v1n, v1m-size*2)
			ap1 = cross(v1, [0,0,1])
			ap2 = cross(v1, [0,0,-1])
			ap1n = norm(ap1)
			ap2n = norm(ap2)
			canvas.create_polygon(x2, y2, x1+ar[0]+ap1n[0]*size,y1+ar[1]+ap1n[1]*size, x1+ar[0]+ap2n[0]*size,y1+ar[1]+ap2n[1]*size, fill=lineColor, tag="edge")

		#Text
		if e[2] is not None:
			canvas.create_text(x2-v1[0]*0.25, y2-v1[1]*0.25, text = str(e[2]), fill=lineColor)

	for n in graph.nodes:
		if n[4]: canvas.create_oval(n[1][0]-nodeRadius, n[1][1]-nodeRadius, n[1][0]+nodeRadius, n[1][1]+nodeRadius, fill=n[3])
		if n[2] is not None:
			canvas.create_text(n[1][0], n[1][1], text = str(n[2]))

def ClearCanvas(canvas):
	for a in canvas.find_all():
		canvas.delete(a)

#Selections
selections = []
def SelectNodes(graph, canvas, nodeIDs, nodeRadius = 20, color="red"):
	if len(nodeIDs) == 0:
		return

	for i in nodeIDs:
		n = graph.nodes[i]
		oval = canvas.create_oval(n[1][0]-nodeRadius, n[1][1]-nodeRadius, n[1][0]+nodeRadius, n[1][1]+nodeRadius, outline=color, width = 4)
		selections.append(oval)

def SelectEdges(graph, canvas, nodeIDs, color="red", width=5):
	#Selects any edges between nodes in nodeIDs
	for i in range(len(nodeIDs)-1):
			id1 = nodeIDs[i]
			id2 = nodeIDs[i+1]
			if id2 in graph.edgesTable[id1] or id1 in graph.edgesTable[id2]:
				n1 = graph.nodes[id1]
				n2 = graph.nodes[id2]
				line = canvas.create_line(n1[1][0], n1[1][1], n2[1][0], n2[1][1], fill=color, width = width, tag="sedge")
				selections.append(line)

	canvas.tag_lower("sedge")
	canvas.tag_lower("edge")

def DeleteSelection(canvas):
	for s in selections:
 		canvas.delete(s)
