from tkinter import *
from Graph import *
from GraphTK import *

#Main containers
root = Tk()
root.title("Graph")
root.wm_geometry("%dx%d+%d+%d" % (650, 650, 25, 25))
canvas = Canvas()
canvas.pack(expand = True, fill=BOTH)

#Setup
nodeRadius = 25
nodeCount = 10
graph = None

def Generate():
	global graph
	graph = Graph()
	graph.CreateRandomNodes(nodeCount, nodeRadius * 2, width=600, height=600)
	graph.CreateCloseEdges(2, nodeRadius+10, directed=True)

	ClearCanvas(canvas)
	DrawGraph(graph, canvas, nodeRadius)

#Control panel setup
frame = Frame()
frame.pack(side=BOTTOM, expand = False, fill=BOTH)

#Widgets
generate=Button(frame, text="Generate", command=Generate)

#Grid layout
Grid.rowconfigure(frame, 0, weight=1)
Grid.columnconfigure(frame, 0, weight=1)
generate.grid(row=0, column=0,sticky=W+S+N+E)

#Key setup
frame.focus_set()
frame.bind("g", lambda x:Generate())

#Run
Generate()
root.mainloop()
