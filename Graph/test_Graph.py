import unittest
from Graph import *

class Test(unittest.TestCase):

	def InitGraph(self):
		g = Graph()
		g.AddNode(0,0,value="A")
		g.AddNode(10,0,value="B")
		g.AddNode(20,0,value="C")
		g.AddNode(30,0,value="D")

		g.AddEdge(0,1,value=1)
		g.AddEdge(1,2,value=2)
		g.AddEdge(2,3,value=3)
		g.AddEdge(3,1,value=4)
		g.AddEdge(3,2,value=5)
		return g

	def test_Graph(self):
		graph = self.InitGraph()
		self.assertEqual(graph.nodeCount(), 4)
		self.assertEqual(graph.edgeCount(), 5)

	def test_Values(self):
		graph = self.InitGraph()
		self.assertEqual(graph.nodes[1][2], "B")
		self.assertEqual(graph.edges[1][2], 2)
