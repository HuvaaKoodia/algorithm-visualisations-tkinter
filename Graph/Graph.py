import random, math
from LineSegmentIntersection import *

class Graph:
	def __init__(self):
		self.nodes = []#Node objects
		self.edges = []#Edge objects
		self.edgesTable=[]#Lists of edge end node indices per node index
		self.edgeIndices=[]#Lists of edge indices in same order as above table

	def nodeCount(self):
		return len(self.nodes)

	def edgeCount(self):
		return len(self.edges)

	def getEdge(self, n1, n2):
		ends = self.edgesTable[n1]
		for i in range(len(ends)):
			if ends[i] == n2:
				return self.edges[self.edgeIndices[i]]
		return None

	def Copy(self, copyEdges=True):
		copy = Graph()
		copy.nodes = []+self.nodes
		if copyEdges:
			copy.edges = []+self.edges
			copy.edgesTable = []+self.edgesTable
			copy.edgeIndices = []+self.edgeIndices
		else:
			copy.edgesTable = [[] for i in range(len(copy.nodes))]
			copy.edgeIndices = [[] for i in range(len(copy.nodes))]

		return copy

	def AddNode(self,x,y,value=None, color="white", drawBG=True):
		index = len(self.nodes)
		self.nodes.append((index, (x, y), value, color, drawBG))
		self.edgesTable.append([])
		self.edgeIndices.append([])
		return index

	def AddEdge(self, i, j, value=None, directed=False, color="black"):
		index = len(self.edges)
		self.edges.append((i,j, value, color))
		self.edgesTable[i].append(j)
		self.edgeIndices[i].append(index)
		if not directed:
			self.edgesTable[j].append(i)
			self.edgeIndices[j].append(index)

	def ChangeNodeValue(self, i, v):
		n = list(self.nodes[i])
		n[2] = v
		self.nodes[i] = tuple(n)

	def ChangeEdgeValue(self, i, v):
		n = list(self.edges[i])
		n[2] = v
		self.edges[i] = tuple(n)

	def DistancePos(self, pos1, pos2):
		return math.sqrt((pos1[0] - pos2[0])**2 + (pos1[1] - pos2[1])**2)

	def DistanceNode(self, nID1, nID2):
		return self.DistancePos(self.nodes[nID1][1], self.nodes[nID2][1])

	def CreateRandomNodes(self, nodeCount = 10, nodeRadius = 10, width=200, height=200, noOverlapCheck = False):
		def randomPos():
			return (random.randint(nodeRadius, width - nodeRadius), random.randint(nodeRadius, height - nodeRadius))

		for i in range(nodeCount):
			if noOverlapCheck:
				pos = randomPos()
			else:
				overlaps = True
				while(overlaps):
					pos = randomPos()
					overlaps = False
					for n in self.nodes:
						if self.DistancePos(pos, n[1]) < (nodeRadius*2):
							overlaps = True
							break

			self.AddNode(pos[0], pos[1], len(self.nodes))

	def CreateNodeLayers(self, layerCount, nodeCountPerLayer, startX, startY, xDifference, yDifference, xVariation=0, yVariation=0, setNodeValuesToIndices=True):
		v = 0
		layers=[]
		for l in range(layerCount):
			layer = []
			layers.append(layer)
			for n in range(nodeCountPerLayer):
				if setNodeValuesToIndices:
					vv=v
				else:
					vv=None
				layer.append(self.AddNode(startX+l*xDifference, startY+n*yDifference, vv))
				v+=1
		return layers

	def CreateCloseEdges(self, maxEdgeCount, nodeRadius, **edgeArgs):
		nodeCount = len(self.nodes)

		for n in self.nodes:
			nID = n[0]
			closestNodes = sorted(self.nodes, key=lambda t:self.DistancePos(t[1], n[1]))[1:]
			childCount = random.randint(1, min(maxEdgeCount, nodeCount - 1))
			c = 0
			while(c < len(closestNodes) and childCount > 0):
				otherID = closestNodes[c][0]
				if not otherID in self.edgesTable[nID]:
					#Check for edge and node *intersections
					canAdd = True
					A = self.nodes[nID][1]
					B = self.nodes[otherID][1]

					for e in self.edges:
						C = self.nodes[e[0]][1]
						D = self.nodes[e[1]][1]
						if LineLineIntersect(A,B,C,D):
							canAdd = False
							break

					if canAdd:
						for oID in range(len(self.nodes)):
							if oID == nID or oID == otherID:
								continue
							if LineCircleIntersect(A,B,self.nodes[oID][1],nodeRadius):
								canAdd = False
								break

					if canAdd:
						#Add edge if no intersections
						self.AddEdge(nID, otherID, **edgeArgs)
						childCount -= 1
				c += 1

	def CreateRandomEdges(self):
		nodeCount = len(self.nodes)
		for n in self.nodes:
			nID = n[0]
			childCount = random.randint(1, nodeCount - 1)
			c = 0
			while(c < nodeCount and childCount > 0):
				otherID = self.nodes[c][0]
				if not otherID in self.edgesTable[nID]:
					self.AddEdge(nID, otherID)
					childCount -= 1
				c += 1

	def CreateRandomLayerEdges(self, layers, edgeCount, randomValues=None, **edgeArgs):
		for i in range(len(layers)-1):
			layer = layers[i]
			nextLayer = layers[i+1]
			edgeCountTable = [0]*len(nextLayer)

			for ni in range(len(layer)):
				n = layer[ni]
				#Available nodes nearby in the next layer, trying to minimize long crossing edges
				if isinstance(edgeCount, tuple):
					eCount = random.randint(edgeCount[0], edgeCount[1])
				else:
					eCount = edgeCount

				count = random.randint(1, min(eCount, len(nextLayer)))
				availableNodes = [i for i in range(max(0, ni-math.ceil(count/2)), min(ni+math.ceil(count/2)+1, len(nextLayer)))]
				count = min(count, len(availableNodes))

				while(count > 0):
					index = random.randint(0, len(availableNodes)-1)
					edgeIndex = availableNodes[index]
					other = nextLayer[edgeIndex]
					if randomValues is not None:
						edgeArgs["value"]=random.randint(randomValues[0], randomValues[1])
					self.AddEdge(n, other, **edgeArgs)
					edgeCountTable[edgeIndex] += 1
					availableNodes.pop(index)
					count -= 1

			#Check if some nodes didn't receive any edges
			for i in range(len(nextLayer)):
				if edgeCountTable[i] > 0: continue
				n = nextLayer[i]
				availableNodes = [j for j in range(len(layer))]
				other = layer[random.randint(0, len(availableNodes)-1)]
				self.AddEdge(other, n, **edgeArgs)
