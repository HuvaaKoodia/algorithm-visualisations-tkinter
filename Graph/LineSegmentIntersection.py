import math

#Line segment intersection adapted from https://bryceboe.com/2006/10/23/line-segment-intersection-algorithm/
def ccw(A,B,C):
	return (C[1]-A[1])*(B[0]-A[0]) > (B[1]-A[1])*(C[0]-A[0])

def LineLineIntersect(A,B,C,D):
	if A == C or A == D or B == C or B == D:
		return False
	return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

#Line segment circle intersection. My own adaption

# Some homebrew vector math

def mul(a,s):
	return (a[0]*s,a[1]*s,a[2]*s)

def div(a,s):
	return (a[0]/s,a[1]/s,a[2]/s)

def add(a,b):
	return (a[0]+b[0],a[1]+b[1],a[2]+b[2])

def sub(a,b):
	return (a[0]-b[0],a[1]-b[1],a[2]-b[2])

def mag(a):
	return math.sqrt(a[0]**2 + a[1]**2)

def cross(a, b):
    return (a[1]*b[2] - a[2]*b[1], a[2]*b[0] - a[0]*b[2], a[0]*b[1] - a[1]*b[0])

def norm(a):
	return div(a, mag(a))

#Line (point A, point B), Circle (center C, radius R)
def LineCircleIntersect(A,B,C,R):
	#Calculate 3D cross products of line and forward and backward vectors
	#The cross products are vectors perpendicular to the line
	line = (A[0]-B[0], A[1]-B[1], 0)
	c1 = cross(line, (0, 0, 1))
	c2 = cross(line, (0, 0, -1))

	#Calculate vector distances for normalization
	d1 = mag(c1)
	d2 = mag(c2)

	#Calculate two new points starting from C
	#going to both perpendicular directions
	#for length of R
	c1 = (C[0]+c1[0]/d1*R, C[1]+c1[1]/d1*R)
	c2 = (C[0]+c2[0]/d2*R, C[1]+c2[1]/d2*R)

	return LineLineIntersect(A,B, c1, c2)
